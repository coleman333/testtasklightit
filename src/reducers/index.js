import { combineReducers } from 'redux';

// import errorReducer from './errorReducer';
// import inventoryReducer from './inventoryReducer';
// import equipmentReducer from './equipmentReducer';
// import markingReducer from './markingReducer';
// import connectionReducer from './connectionReducer';
import authReducer from './authReducer';
// import testHook from './testHook';
// import { authReducerSaga } from "../store/SagaReducers/auth";
// import sagaReducers  from '../store/SagaReducers/inventory';

export default combineReducers({
  auth: authReducer,
  // error: errorReducer,
  // inventory: inventoryReducer,
  // equipment: equipmentReducer,
  // marking: markingReducer,
  // connection: connectionReducer,
  // testHook: testHook,

  // extended: authReducerSaga,

  // sagaReducers: sagaReducers
});
