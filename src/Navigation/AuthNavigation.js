import { createAppContainer } from 'react-navigation';
import React from 'react';
import { createStackNavigator } from '@react-navigation/stack';
import  Screen from 'react-native-screens';
import RegisterPage from '../Views/RagisterPage';
// import ForgotPassword from '../Views/ForgotPasswordPage';
// import AuthCamera from '../Views/AuthCamera';

const Stack = createStackNavigator();

function AuthNavigation() {
  return (
    <Stack.Navigator>
      <Stack.Screen name="RegisterPage" component={RegisterPage} />


    </Stack.Navigator>
  );
}

// const AuthNavigation = createStackNavigator(
//   {
//     RegisterPage,
//     // ForgotPassword,
//     // AuthCamera
//   },
//   {
//     headerMode: 'none'
//   },
//   {
//     initialRouteName: RegisterPage,
//     // navigationOptions: { header: null },
//   },
// );

export default createAppContainer(AuthNavigation);
