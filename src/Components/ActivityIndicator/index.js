import React from 'react';
import { View, ActivityIndicator } from 'react-native';

import Colors from '../../Constants/Colors';

export default () => (
  <View
    style={{ flex: 1, alignItems: 'center', justifyContent: 'center' }}
  >
    <ActivityIndicator
      size="large"
      color={Colors.mhp_blue}
    />
  </View>
);
