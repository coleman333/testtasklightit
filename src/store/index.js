import { createStore, applyMiddleware, compose } from 'redux';
import thunk from 'redux-thunk';
// import sagas from './Sagas';

import rootReducer from '../reducers/index';
// import sagaReducer from '../store/SagaReducers'
import createSagaMiddleware from 'redux-saga';
const sagaMiddleWare = createSagaMiddleware();


const initialState = {};
const middleware = [thunk, sagaMiddleWare ];
// const middleware = [thunk ];
const composeEnhancers = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ ? window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__: compose;
const store = createStore(
  rootReducer,
  // sagaReducer,
  initialState,
  composeEnhancers(
    applyMiddleware(...middleware)
  )
);

export default store;

// sagaMiddleWare.run(sagas);
