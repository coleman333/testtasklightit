import React, {Fragment} from 'react';
import { StyleSheet, ScrollView, View, Text, StatusBar } from 'react-native';

import {
  Header,
  LearnMoreLinks,
  Colors,
  DebugInstructions,
  ReloadInstructions,
} from 'react-native/Libraries/NewAppScreen';
import SafeAreaView from 'react-native-safe-area-view';


import { Provider } from 'react-redux';
import Navigator from './src/Navigation/Router';
import SwitchNavigator from './src/Navigation/SwitchNavigator';
import { AsyncStorage } from 'react-native';
import NetInfo from "@react-native-community/netinfo";
import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';

// import { Sentry } from 'react-native-sentry';
// Sentry.config('https://95c05ead19084c389645bc66c440550e@sentry.io/1551472').install();


import store from './src/store';
import RegisterPage from './src/Views/RagisterPage';
import Identification from './src/Views/Identification';



// import _Modal from './Src/Components/Modal';

// import { CHECK_CONNECTION } from "./actions/types";
// import { mountInventory, createTransaction } from "./actions/equipmentAction";

const App = () => {
  const Stack = createStackNavigator();

  let isLoggedIn = true;
  return (
    <Fragment>
      {/*<StatusBar barStyle="dark-content" />*/}
      <View>


          {/*<StatusBar barStyle="dark-content" />*/}
          {/*<SafeAreaView>*/}
          {/*<_Modal*/}
          {/*closeModal={this.closeModal}*/}
          {/*visible={this.state.lostConnection}*/}
          {/*type={'alert'}*/}
          {/*>*/}
          {/*Проверьте соединение с интернетом!*/}
          {/*</_Modal>*/}

          {/*<_Modal*/}
          {/*closeModal={this.closeModal}*/}
          {/*visible={this.state.getConnection}*/}
          {/*type={'alert'}*/}
          {/*>*/}
          {/*Соединение с интернетом установлено!*/}
          {/*</_Modal>*/}


          {/*<Text>;alksdjf;</Text>*/}

          <Provider store={store} >
            <NavigationContainer>
              {/*<SwitchNavigator />*/}
              <Stack.Navigator>
                {isLoggedIn ? (
                  <>
                    <Stack.Screen name="Identification" component={Identification} />
                  </>
                ) : (

                  <Stack.Screen name="RegisterPage" component={RegisterPage} />

                )}
              </Stack.Navigator>
            </NavigationContainer>

          </Provider>
      </View>
    </Fragment>
  );
};

const styles = StyleSheet.create({
  scrollView: {
    backgroundColor: Colors.lighter,
  },
  engine: {
    position: 'absolute',
    right: 0,
  },
  body: {
    backgroundColor: Colors.white,
  },
  sectionContainer: {
    marginTop: 32,
    paddingHorizontal: 24,
  },
  sectionTitle: {
    fontSize: 24,
    fontWeight: '600',
    color: Colors.black,
  },
  sectionDescription: {
    marginTop: 8,
    fontSize: 18,
    fontWeight: '400',
    color: Colors.dark,
  },
  highlight: {
    fontWeight: '700',
  },
  footer: {
    color: Colors.dark,
    fontSize: 12,
    fontWeight: '600',
    padding: 4,
    paddingRight: 12,
    textAlign: 'right',
  },
});

export default App;
