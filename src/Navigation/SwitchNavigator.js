import React from 'react';
import { createAppContainer, createSwitchNavigator } from 'react-navigation';
import mainNavigator from "./Router";
import  AuthNavigation  from "./AuthNavigation";
// import RegisterPage from '../Views/RagisterPage';



const SwitchNavigator = createSwitchNavigator({
    // Navigator: Navigator,
    // splashScreen: drawerNavigator

    AuthNavigation,
    // mainNavigator: mainNavigator
  },{
    navigationOptions: {
      header: null,
    },
  }
);

export default createAppContainer(SwitchNavigator);
