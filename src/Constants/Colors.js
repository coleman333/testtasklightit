const colors = {
  dark_gray: '#4a4a4a',
  gray: '#856F6F',
  light_gray: '#b2b2b2',
  xtra_light_gray: '#ececec',
  aquamarine: '#45c7cd',
  fuchsia: '#e6007e',
  light_fuchsia: '#f984ef',
  green: '#bbd030',
  orange: '#fddb2f',
  blue: '#3b5998',
  red: '#dd4b39',
  blackGray: '#34393F',
  white: '#FFFFFF',
  black: '#34393F',
  grayBorder: '#856F6F',
  BlueWay: '#01B2BB',
  tabBorderGray: '#D8D8D8',
  tabBorderPurple: '#773E91',
  selectBlack: '#444444',
  blueButton: '#007AFF',
  metricGray: '#5A6175',
  purple: '#4b0082',
  light_purple: '#9370db',
  success: '#60B44F',
  whiteGray: '#D8D8D8',

  mhp_green: '#a9b640',
  mhp_blue: '#003a5d',
  mhp_purple: '#856F6F'
};

export default colors;
