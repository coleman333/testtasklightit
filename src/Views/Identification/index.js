import React, { Component, useEffect, useState } from 'react';
import { View, TextInput, TouchableOpacity, Keyboard } from 'react-native';
import SafeAreaView from 'react-native-safe-area-view';
import { connect, useDispatch, useSelector } from 'react-redux';

import { Button } from '../../Components/Button';
import Header from '../../Components/Header';
import Modal from '../../Components/Modal';
import Spinner from '../../Components/ActivityIndicator';
import { isAuth, registrationAction } from '../../actions/AuthAction';
// import { getTime } from '../../../actions/inventoryAction';
// import { closeError } from '../../../actions/errorAction';
// import { auth, toolsTypes } from '../../../store/SagaTypes/serviceTypes';
import auth from '../../Constants/types'

import styles from './styles';

const Identification = (props) => {

  const { navigation } = props;
  const dispatch = useDispatch();
  // const token = useSelector((state) => state.authReducer.token);
  // const connection = useSelector((state) => state.toolsReducer.connection);

  const [inProgress, setProgress] = useState( false );
  const [email, setEmail] = useState( 'administrator@gmail.com' );
  let [id, setId] = useState( '' );
  const [modal, setModal] = useState( null );
  const [connectionModal, setConnectionModal] = useState( null );
  const [serverError, setServerError] = useState( null );
  const [ notFoundModal, setNotFoundModal] = useState( false );
  // const toolNotFound = useSelector((state) => state.toolsReducer.toolNotFound);


  // // if no any connection
  // useEffect(() => {
  //     // if (connection) {
  //       setConnectionModal(false);
  //       setProgress(false);
  //       // requestAnimationFrame( ()=> navigation.navigate('Identification'));
  //     // }
  //     // else{
  //     //   setConnectionModal(true);
  //     //   setProgress(false);
  //     // }
  //   },[connection]
  // );


  // useEffect(()=>{
  //   if(toolNotFound ){
  //     setNotFoundModal(true)
  //   }
  // },[toolNotFound]);

  //  open camera to scan QR-code
  const openCamera = () => {
    console.log('this is from receive info');
    navigation.navigate('IdentificationCamera',
      { text: 'Отсканируйте QR-код', type: 'search', page: 'Identification' });
  };

  const getRidOfSpecialElements = () => {
    id = id.toLowerCase();
    return id.replace(/[&\/\\#,+()$~%. '":*?<>{}]/g, '');
  };

  //  get info about material or equipment according to id
  const getInfo = () => {
    if (id.length) {
      // if (connection) {
      setProgress(true);
      //   setId( getRidOfSpecialElements() );
        setId( getRidOfSpecialElements() );
        switch(id[0]){
          case 't':{
            // const data = {
            //   type: toolsTypes.getToolById,
            //   payload: {id , navigation: props.navigation}
            // };
            // dispatch(data);
            // searchActionFunc(id, 'identify_id', false, navigation);
          }
            // setProgress(false);
            break;
          // case 'i':{
          //   searchActionFunc(id, 'identify_id', false, navigation);
          // }
          //   break;
          // case 'w': {
          //   searchWarehouseSharpeningActionFunc(id, 'identify_warehouse_id', false, navigation);
          // }
          //   break;
          // case 'r': {
          //   searchWarehouseSharpeningActionFunc(id, 'identify_sharpening_id', false, navigation);
          // }
          // break;
          default: {
            // this.setState({identifyWarehouseIdError: true})
          }
            break;
        }
      setProgress(false);

      // } else {
      //   this.setState({
      //     connectionModal: true
      //   });
      // }
    }
  };

  //  close modal window
  const closeModal = () => {
    setId('');
    setServerError(false);
    setNotFoundModal(false);
    // this.setState({
    //   identifyWarehouseIdError: false,
    //   identifySharpeningIdError: false,
    // });

    // props.closeError();
  };

  //  get value from input
  const getValue = (e) => {
    console.log('asdfasdgdsghdfghdfghdfgh', e);
    setId(e);
    console.log('ssssssssssssssssssssssssssssss', id);
  };

    return (
      <View
        style={styles.container}
      >
        <TouchableOpacity
          activeOpacity={1.0}
          onPress={Keyboard.dismiss}
          style={{ flex: 1 }}
        >
          <React.Fragment>
            {/*<Modal*/}
              {/*closeModal={closeModal}*/}
              {/*visible={notFoundModal}*/}
              {/*type="alert"*/}
            {/*>*/}
              {/*{`Идентификатор ${id} не найден в базе!`}*/}
            {/*</Modal>*/}
            {/*<Modal*/}
              {/*closeModal={this.closeModal}*/}
              {/*visible={this.state.identifyWarehouseIdError || this.state.identifySharpeningIdError}*/}
              {/*type="alert"*/}
            {/*>*/}
              {/*{`Идентификатор ${this.state.id} не найден в базе!`}*/}
            {/*</Modal>*/}
            {/*<Modal*/}
              {/*closeModal={closeModal}*/}
              {/*visible={connectionModal}*/}
              {/*type="alert"*/}
            {/*>*/}
              {/*Отсутствует интернет соединение*/}
            {/*</Modal>*/}


            {/*<Modal*/}
              {/*closeModal={ closeModal}*/}
              {/*visible={ serverError}*/}
              {/*type="alert"*/}
            {/*>*/}
              {/*Сервер не отвечает*/}
            {/*</Modal>*/}
            {/*<Header>*/}
              {/*Идентификация ТМЦ*/}
            {/*</Header>*/}
            {
              inProgress
                ? <Spinner />
                : (
                  <React.Fragment>
                    <View
                      style={styles.item}
                    >
                      <View
                        style={styles.inputWithButton}
                      >
                        <TextInput
                          value={ id }
                          onChangeText={getValue}
                          placeholder="введите идентификатор"
                          style={styles.input}
                        />
                        <Button
                          onClick={getInfo}
                        >
                          Отправить
                        </Button>
                      </View>
                    </View>
                    <View
                      style={styles.item}
                    >
                      <Button
                        onClick={openCamera}
                      >
                        Открыть камеру
                      </Button>
                    </View>
                  </React.Fragment>
                )
            }
          </React.Fragment>
        </TouchableOpacity>
      </View>
    );
};

export default Identification;
