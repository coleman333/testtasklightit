import React, { Component } from 'react';
import {
  View, Text, TextInput, AsyncStorage
} from 'react-native';

import Svg, { G, Path } from 'react-native-svg';
import PropTypes from 'prop-types';
import styles from './styles';
import { Button } from '../Button';
import Colors from '../../Constants/Colors';

export default class Prompt extends Component {
  constructor (props) {
    super(props);
    this.state = {
      host: '',
      port: ''
    };
    this.confirm = this.confirm.bind(this);
    this.getHostValue = this.getHostValue.bind(this);
    this.getPortValue = this.getPortValue.bind(this);
  }

  componentDidMount () {
    AsyncStorage.getItem('host')
      .then((data) => {
        if (data !== null) {
          this.setState({
            host: data
          });
        }
      })
      .catch(() => {

      });
    AsyncStorage.getItem('port')
      .then((data) => {
        if (data !== null) {
          this.setState({
            port: data
          });
        }
      })
      .catch(() => {

      });
  }

  getHostValue (e) {
    this.setState({
      host: e
    });
  }

  getPortValue (e) {
    this.setState({
      port: e
    });
  }

  confirm () {
    const { port, host } = this.state;
    const { onClick } = this.props;
    AsyncStorage.setItem('host', host);
    AsyncStorage.setItem('port', port);
    onClick();
  }

  render () {
    const { port, host } = this.state;
    const { children } = this.props;
    return (
      <View
        style={styles.prompt}
      >
        <View
          style={styles.imageBlock}
        >
          <Svg viewBox="0 0 52 52" width="90" height="90">
            <G>
              <Path d="M26,0C11.664,0,0,11.663,0,26s11.664,26,26,26s26-11.663,26-26S40.336,0,26,0z M26,50C12.767,50,2,39.233,2,26   S12.767,2,26,2s24,10.767,24,24S39.233,50,26,50z" fill="#e47b7b" />
              <Path d="M26,10c-0.552,0-1,0.447-1,1v22c0,0.553,0.448,1,1,1s1-0.447,1-1V11C27,10.447,26.552,10,26,10z" fill="#e47b7b" />
              <Path d="M26,37c-0.552,0-1,0.447-1,1v2c0,0.553,0.448,1,1,1s1-0.447,1-1v-2C27,37.447,26.552,37,26,37z" fill="#e47b7b" />
            </G>
          </Svg>
        </View>
        <View
          style={styles.textBlock}
        >
          <Text
            style={styles.text}
          >
            {children}
          </Text>
        </View>
        <View
          style={{ flex: 2 }}
        >
          <TextInput
            value={host}
            onChangeText={this.getHostValue}
            placeholder="ip-адресс"
            style={styles.input}
          />
          <TextInput
            value={port}
            onChangeText={this.getPortValue}
            placeholder="порт"
            style={styles.input}
          />
        </View>
        <View
          style={styles.buttonBlock}
        >
          <Button
            onClick={this.confirm}
            buttonStyle={{
              width: '30%',
              backgroundColor: Colors.success
            }}
            textStyle={{
              textAlign: 'center',
              color: Colors.white
            }}
          >
            OK
          </Button>
        </View>
      </View>
    );
  }
}

Prompt.defaultProps = {
  onClick () {

  },
  children: ''
};

Prompt.propTypes = {
  onClick: PropTypes.func,
  children: PropTypes.string
};
