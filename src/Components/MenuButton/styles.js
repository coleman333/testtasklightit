import { StyleSheet } from 'react-native';

export default StyleSheet.create({
  menuButton: {
    alignItems: 'flex-start',
    position: 'absolute',
    left: 0,
    top: 15
  }
});
