import { StyleSheet } from 'react-native';
import colors from '../../Constants/Colors';
import { fontSize } from '../../Constants/Text';

export default StyleSheet.create({
  header: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'center',
    borderBottomWidth: 1,
    borderBottomColor: colors.grayBorder,
    paddingVertical: 20,
  },
  containerForHeaderText: {
    flexDirection: 'column',
    alignItems: 'center',
    justifyContent: 'center',
    borderBottomColor: colors.grayBorder,

  },
  headerForMountingPromptsPage :{
    flexDirection: 'column',
    color: colors.gray,
    fontSize: fontSize.small,
    justifyContent: 'center',
    textAlign: 'center'
  },
  headerText: {
    color: colors.gray,
    fontSize: fontSize.small,
    justifyContent: 'center',
    textAlign: 'center'
  }
});
