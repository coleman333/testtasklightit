import { StyleSheet } from 'react-native';

export default StyleSheet.create({
  menuButton: {
    alignItems: 'flex-start',
    position: 'absolute',
    right: 0,
    top: 15
  }
});
