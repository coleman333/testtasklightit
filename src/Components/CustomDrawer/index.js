import React from 'react';
import { ScrollView, Image, TouchableOpacity, View } from 'react-native';
import { DrawerItems, StackActions, NavigationActions } from 'react-navigation';
import SafeAreaView from 'react-native-safe-area-view';

export default props => (
  <View style={{ flex: 1, backgroundColor: 'white', borderBottomRightRadius: 25, borderTopRightRadius: 25 }}>
    <View>
    <Image style={{marginLeft:40, width: 120, height: 60}} source={require('../../assets/img/logo.png')} />
      <View style={{ marginLeft: 10, marginRight: 10, borderBottomColor: 'black', borderBottomWidth: 1}}/>
      <DrawerItems
        // itemStyle={{marginLeft: 10, marginRight: 10, borderBottomColor: 'black', borderBottomWidth: 1}}
        // activeLabelStyle={{marginLeft: 10, marginRight: 10, borderBottomColor: 'black', borderBottomWidth: 1}}
        {...props}
        onItemPress={
          (router) => {
            console.log('new screen');
            const resetAction = StackActions.reset({
              index: 0,
              actions: [NavigationActions.navigate({ routeName: router.route.routeName })]
            });
            const navigateAction = NavigationActions.navigate({
              routeName: router.route.routeName
            });
            props.navigation.dispatch(navigateAction);
            props.navigation.dispatch(resetAction);
          }
        }
      />
    </View>
  </View>
);
